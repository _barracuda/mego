import Vue from 'vue';
import VueHead from 'vue-head';
import ElementUI from 'element-ui';
import ElementUILocale from 'element-ui/lib/locale/lang/en';
import { sync } from 'vuex-router-sync';
import router from 'router';
import store from 'store';
import App from 'components/app';

import 'element-ui/lib/theme-default/index.css';
import 'style/index';
import 'locales';

sync(store, router, { moduleName: 'router' });

Vue.use(ElementUI, { locale: ElementUILocale });
Vue.use(VueHead);

new Vue({
  router,
  store,
  ...App
}).$mount('#app');
