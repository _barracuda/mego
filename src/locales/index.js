import Vue from 'vue';
import VueI18n from 'vue-i18n';
import en from 'locales/en';
import ru from 'locales/ru';

Vue.use(VueI18n);

Vue.config.lang = 'ru';

const locales = {
  en,
  ru
};

Object.keys(locales).forEach((lang) => {
  Vue.locale(lang, locales[lang]);
});

