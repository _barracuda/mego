import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from 'views/home';
import Feedback from 'views/feedback';
import NotFound from 'views/not-found';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    component: Home
  },
  {
    path: '/feedback',
    component: Feedback
  },
  {
    path: '*',
    component: NotFound
  }
];

export default new VueRouter({
  mode: 'history',
  routes
});