import Vue from 'vue';
import Vuex from 'vuex';
import mutations from 'store/mutations';
import actions from 'store/actions';
import modules from 'store/modules';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: APP_ENV === 'development',
  mutations: mutations,
  actions: actions,
  modules: modules
});
