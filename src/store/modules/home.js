import api from 'store/api';

export default {
  namespaced: true,
  state: {
    ads: [],
    cursor: 1,
    countAds: 0,
    isEnd: false,
    realtyTypes: [
      'flat',
      'room',
      'house'
    ],
    settlements: [],
    filter: {
      type: 'buy',
      realty: 'flat',
      settlementId: []
    },
    loading: {
      ads: false,
      settlements: false
    }
  },
  mutations: {
    ADD_AD_LIST (state, payload) {
      state.ads.push(payload);
    },
    INCREMENT_CURSOR (state) {
      state.cursor++;
    },
    INCREMENT_COUNT_ADS (state) {
      state.countAds++;
    },
    SET_END_AD_LIST (state) {
      state.isEnd = true;
    },
    SET_SETTLEMENTS (state, payload) {
      state.settlements = payload;
    },
    SET_FILTER (state, payload) {
      state.filter = {
        ...state.filter,
        ...payload
      };
    },
    SET_LOADING (state, payload) {
      state.loading = {
        ...state.loading,
        ...payload
      };
    },
    RESET_STATE (state) {
      state.ads = [];
      state.cursor = 1;
      state.countAds = 0;
      state.isEnd = false;
    }
  },
  actions: {
    getAds ({ state, commit }) {
      let params = {
        limit: 3,
        cursor: state.cursor,
        filter: state.filter
      };
      commit('SET_LOADING', { ads: true });
      api.get('/ads', { params })
        .then(res => {
          let ads = res.data.items;
          for (let ad of ads) {
            commit('ADD_AD_LIST', ad);
            commit('INCREMENT_COUNT_ADS');
          }
          if (state.countAds === res.data.count) {
            commit('SET_END_AD_LIST');
          }
          commit('INCREMENT_CURSOR');
          commit('SET_LOADING', { ads: false });
        })
        .catch(console.warn.bind(console));
    },
    getSettlements ({ commit }) {
      commit('SET_LOADING', { settlements: true });
      api.get('/settlements')
        .then(res => {
          commit('SET_SETTLEMENTS', res.data.items);
          commit('SET_LOADING', { settlements: false });
        })
        .catch(console.warn.bind(console));
    },
    setFilter ({ dispatch, commit }, payload) {
      commit('SET_FILTER', payload);
      commit('RESET_STATE');
      dispatch('getAds');
    }
  }
};
