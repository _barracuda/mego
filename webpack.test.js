const webpack = require('webpack');
require('assembly/pathes');

module.exports = {
  resolve: {
    extensions: ['', '.js', '.vue', '.css', '.styl'],
    modules: [
      pathes.SRC,
      'node_modules'
    ],
    alias: {
      root: pathes.SRC,
      components: pathes.SRC_COMPONENTS,
      views: pathes.SRC_VIEWS,
      elements: pathes.SRC_ELEMENTS,
      style: pathes.SRC_STYLE,
      store: pathes.SRC_STORE,
      static: pathes.SRC_STATIC,
      locales: pathes.SRC_LOCALES,
      vue$: 'vue/dist/vue.common.js'
    }
  },
  module: {
    rules: [
      {
        test: /\.js$|\.vue$/,
        enforce: 'pre',
        exclude: /node_modules/,
        loader: 'eslint-loader',
        option: {
          configFile: './.eslintrc'
        }
      },
      {
        test: /\.styl$/,
        enforce: 'pre',
        exclude: /node_modules/,
        loader: 'stylint-loader'
      },
      {
        test: /\.html$/,
        loader: 'html-loader'
      },
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      {
        test: /\.vue$/,
        exclude: /node_modules/,
        loader: 'vue-loader',
        options: {
          loaders: {
            styl: ExtractTextPlugin.extract({
              use: [
                {
                  loader: 'css-loader',
                  options: { sourceMap: true }
                },
                {
                  loader: 'stylus-loader',
                  options: { sourceMap: true }
                }
              ]
            }),
            js: 'babel-loader'
          }
        }
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: { sourceMap: true }
            }
          ]
        })
      },
      {
        test: /\.styl$/,
        exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: { sourceMap: true }
            },
            {
              loader: 'stylus-loader',
              options: { sourceMap: true }
            }
          ]
        })
      },
      {
        test: /\.(png|gif|svg|jp(e)?g)(\?\S*)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: 'images/[hash].[ext]',
              limit: 512
            }
          },
          {
            loader: 'image-webpack-loader',
            options: {
              progressive: true,
              mozjpeg: {
                quality: 65
              },
              pngquant:{
                optimizationLevel: 7,
                quality: "65-90",
                speed: 4
              },
              svgo:{
                plugins: [
                  { removeViewBox: false },
                  { removeEmptyAttrs: false }
                ]
              }
            }
          }
        ]
      },
      {
        test: /\.(ttf|eot|woff|woff2)(\?\S*)?$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              name: 'fonts/[hash].[ext]',
              limit: 50000
            }
          }
        ]
      }
    ]
  }
};
