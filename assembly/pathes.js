const path = require('path');

let pathes = {
  ROOT: path.resolve(__dirname, '..')
};

pathes.SRC = path.resolve(pathes.ROOT, 'src');
pathes.SRC_STATIC = path.resolve(pathes.SRC, 'static');
pathes.SRC_STYLE = path.resolve(pathes.SRC, 'style');
pathes.SRC_STORE = path.resolve(pathes.SRC, 'store');
pathes.SRC_VIEWS = path.resolve(pathes.SRC, 'views');
pathes.SRC_COMPONENTS = path.resolve(pathes.SRC, 'components');
pathes.SRC_ELEMENTS = path.resolve(pathes.SRC, 'elements');
pathes.SRC_LOCALES = path.resolve(pathes.SRC, 'locales');
pathes.DIST = path.resolve(pathes.ROOT, 'dist');

global.pathes = pathes;