const config = require('dotenv-safe').load({
  allowEmptyValues: true,
  sample: '.env.default'
});

global.isDev = process.env.APP_ENV === 'development';
global.isProd = process.env.APP_ENV === 'production';

let definitions = {};
for (let i in config.required)
  definitions[i] = JSON.stringify(config.required[i]);

module.exports = definitions;
