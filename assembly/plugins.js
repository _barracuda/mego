const webpack = require('webpack');
const path = require('path');
const definitions = require('assembly/definitions');

// IMPORT WEBPACK PLUGIN
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

let commonPlugins = [
  new webpack.DefinePlugin(definitions),
  new webpack.optimize.CommonsChunkPlugin({ name: 'vendor' }),
  new CleanWebpackPlugin('dist', { root: pathes.ROOT }),
  new HtmlWebpackPlugin({
    title: process.env.APP_NAME,
    filename: 'index.html',
    template: '!!mustache-loader!' + path.join(pathes.SRC, 'index.html')
  }),
  new ExtractTextPlugin(isDev ? '[name].[chunkhash].css' : '[name].min.css')
];

let developmentPlugins = [];
if (isDev) {
  developmentPlugins = [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.SourceMapDevToolPlugin({
      filename: '[file].map',
      exclude: ['vendor']
    })
  ];
}

let productionPlugins = [];
if (isProd) {
  productionPlugins = [
    new webpack.optimize.UglifyJsPlugin({
      compress: { warnings: false },
      comments: false,
      drop_debugger: true,
      unused: true,
      unsafe: true,
      sequences: true,
      booleans: true,
      loops: true
    }),
    new FaviconsWebpackPlugin({
      logo: path.join(pathes.SRC_STATIC, 'favicon.png'),
      persistentCache: true,
      inject: true,
      background: '#fff',
      title: 'Webpack App',
      icons: {
        android: false,
        appleIcon: false,
        appleStartup: false,
        coast: false,
        favicons: true,
        firefox: false,
        opengraph: false,
        twitter: false,
        yandex: false,
        windows: false
      }
    }),
    new OptimizeCssAssetsPlugin({
      cssProcessor: require('cssnano'),
      cssProcessorOptions: { discardComments: { removeAll: true } },
      canPrint: true
    })
  ];
}

module.exports = [...commonPlugins, ...developmentPlugins, ...productionPlugins];
