
module.exports = {
  contentBase: pathes.SRC,
  open: false,
  hot: true,
  inline: true,
  historyApiFallback: true,
  compress: true,
  clientLogLevel: 'info',
  watchOptions: {
    aggregateTimeout: 300,
    poll: 1000
  },
  quiet: false,
  noInfo: false,
  stats: { colors: true },
  port: process.env.APP_PORT
};