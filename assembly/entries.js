
module.exports = {
  app: './index.js',
  vendor: [
    'vue',
    'vue-router',
    'vue-head',
    'vuex',
    'axios',
    'element-ui'
  ]
};
