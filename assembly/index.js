
module.exports = {
  pathes: require('./pathes'),
  definitions: require('./definitions'),
  entries: require('./entries'),
  plugins: require('./plugins'),
  loaders: require('./loaders'),
  devServer: require('./dev-server')
};