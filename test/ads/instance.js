import Vue from 'vue';
import Vuex from 'vuex';
import Ads from 'components/ads';
import mockedStore from 'test/ads/mock-store';

Vue.use(Vuex);

export default new Vue({
  template: '<div><ads/></div>',
  components: {
    ads: Ads
  },
  store: new Vuex.Store(mockedStore)
});
