import Vue from 'vue';
import app from 'test/ads/instance';
import chai from 'chai';
import chaiDom from 'chai-dom';

chai.use(chaiDom);
const expect = chai.expect;

/* global describe it */

describe('Ads', () => {
  it('renders the correct message', done => {
    const vm = app.$mount();

    vm.$children[0].increment();

    Vue.nextTick(() => {
      expect(vm.$el.querySelector('h4')).to.have.text('Список объявлений');
      expect(vm.$el.querySelector('.count')).to.have.text('Количество: 1');
      done();
    });
  });
});
