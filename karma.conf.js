const webpackConfig = require('./webpack.test.js');

module.exports = config => {
  config.set({
    files: ['node_modules/babel-polyfill/dist/polyfill.js', 'test/**/*.spec.js'],
    frameworks: ['mocha'],
    preprocessors: {
      'test/**/*.spec.js': ['webpack']
    },
    reporters: ['mocha'],
    webpack: webpackConfig,
    webpackMiddleware: {
      noInfo:true
    },
    browsers: ['PhantomJS']
  });
};