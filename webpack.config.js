const assembly = require('assembly');

module.exports = {
  context: pathes.SRC,
  entry: assembly.entries,
  output: {
    filename: isDev ? '[name].[id].[hash].js' : '[name].min.js',
    chunkFilename: isDev ? "[id].[hash].js" : '[id].min.js',
    path: pathes.DIST
  },
  plugins: assembly.plugins,
  module: assembly.loaders,
  resolve: {
    extensions: ['*', '.js', '.vue', '.css', '.styl'],
    modules: [
      pathes.SRC,
      'node_modules'
    ],
    alias: {
      root: pathes.SRC,
      components: pathes.SRC_COMPONENTS,
      views: pathes.SRC_VIEWS,
      elements: pathes.SRC_ELEMENTS,
      style: pathes.SRC_STYLE,
      store: pathes.SRC_STORE,
      static: pathes.SRC_STATIC,
      locales: pathes.SRC_LOCALES,
      vue$: 'vue/dist/vue.common.js'
    }
  },
  devServer: assembly.devServer
};